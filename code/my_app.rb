# This file represents "software".
# This file helps to show that documentation files can be in the same repository as software.

def my_method(&my_block)
  puts "We're in the method, invoking block next!"
  my_block.call
  puts "We're back in the method!"
end

my_method  do
  puts "We're in the block!"
end
